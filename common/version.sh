#!/usr/bin/env bash
#

__repo__='https://gitlab.com/bschaves/shell-libs'
__online_pkg__='https://gitlab.com/bschaves/shell-libs/-/archive/main/shell-libs-main.zip'
__version__='2.0.3'
__appname__='shell-libs'