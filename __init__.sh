#!/usr/bin/env bash
#
# 
# Esse arquivo inicializa variáveis, e o core de funções shell-libs
# 
#
#

[[ -z $SHELL_LIBS ]] && {
	echo "ERRO ... diretório SHELL_LIBS não encontrado."
	exit 1
}


SHELL_DEVICE_FILE="/tmp/$(whoami)/device.txt"
SHELL_STATUS_FILE="/tmp/$(whoami)/status.txt"
export STATUS_OUTPUT=0

mkdir -p /tmp/$(whoami)
touch $SHELL_DEVICE_FILE
touch $SHELL_STATUS_FILE

source ~/.bashrc

[[ -z $HOME ]] && HOME=~/

# Inserir ~/.local/bin em PATH se não existir.
echo "$PATH" | grep -q "$HOME/.local/bin" || {
	export PATH="$HOME/.local/bin:$PATH"
}

# Inserir ~/bin em PATH se não existir.
echo "$PATH" | grep -q "$HOME/bin" || {
	export PATH="$HOME/bin:$PATH"
}

#
source "${SHELL_LIBS}"/common/print_text.sh || exit 1
source "${SHELL_LIBS}"/common/string.sh || exit 1
source "${SHELL_LIBS}"/common/utils.sh || exit 1
source "${SHELL_LIBS}"/common/version.sh || exit 1
#
source "${SHELL_LIBS}"/system/sys.sh || exit 1
source "${SHELL_LIBS}"/system/apt-bash.sh || exit 1

if [[ $(id -u) == 0 ]]; then
	source "${SHELL_LIBS}"/system/_dirs_root.sh || exit 1
else
	source "${SHELL_LIBS}"/system/_dirs_user.sh || exit 1
	createUserDirs
	chmod +x "${SHELL_LIBS}"/scripts/user-path.sh
	"${SHELL_LIBS}"/scripts/user-path.sh
fi
